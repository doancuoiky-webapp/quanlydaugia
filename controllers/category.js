const express = require('express');
const router = express.Router();
const mCat = require('../models/category');
const mPro = require('../models/product');

router.get('/', async (req, res) => {
    const cats = await mCat.all();
    for (let cat of cats) {
        cat.isActive = false;
    }
    cats[0].isActive = true;
    res.render('home', {
        title: 'CUA HANG DIEN TU',
        cats: cats,
    });
});

router.get('/:id/products', async (req, res) => {
    const id = parseInt(req.params.id);
    const page = req.query.page || 1;
    const cats = await mCat.all();
    const rs = await mPro.allByCatIDPaging(id, page);
    for (let cat of cats) {
        cat.isActive = false;
        if (cat.CatID === id) {
            cat.isActive = true;
        }
    }
    const pages = [];
    for (let i = 0; i < rs.pageTotal; i++) {
        pages[i] = {
            value: i + 1,
            active: (i + 1) == page,
        };
    }
    const navs = {};
    if (page > 1) {
        navs.prev = page - 1;
    }
    if (page < rs.pageTotal) {
        navs.next = page + 1;
    }
    res.render('home', {
        title: 'Products',
        cats: cats,
        ps: rs.products,
        pages: pages,
        navs: navs,
    });
});

module.exports = router;