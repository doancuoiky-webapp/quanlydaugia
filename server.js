const express = require('express'),
    app = express(),
    port = 3000,
    exphbs = require('express-handlebars');

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.render('home');
});
app.use('/cat', require('./controllers/category'));
app.listen(port, () => console.log(`Example app listening on port ${port}!`));