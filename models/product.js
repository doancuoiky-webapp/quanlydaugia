const db = require('../untils/db');
const tbName = 'Products';
const pagesize = 6;

module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    allByCatID: async id => {
        const sql = `SELECT * FROM ${tbName} WHERE CatID=${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    allByCatIDPaging: async (id, page) => {
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE CatID=${id}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pagesize) + 1;
        const offset = (page - 1) * pagesize;
        sql = `SELECT * FROM ${tbName} WHERE CatID=${id} LIMIT ${pagesize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows,
        };
    },
};